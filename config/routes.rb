Rails.application.routes.draw do
	# The root application route
  root 'welcome#index'

  controller :welcome do
	  get :home
	  get :about
	  get :contact
	end
  
  # vendors and products routes
  resources :vendors
  resources :products




  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

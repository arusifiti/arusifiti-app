require 'test_helper'

class WelcomeControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get root_path
    assert_response :success
    assert_select "title", "Arusifiti For the Perfect wedding"
  end

  test "should get about" do
  	get about_path 
  	assert_response :success 
  	assert_select "title", "About • Arusifiti For the Perfect wedding"
  end

  test "should get contact" do
  	get contact_path 
  	assert_response :success
  	assert_select "title", "Contact • Arusifiti For the Perfect wedding"
  	 
  end

  # test "should get home" do
  #   get static_pages_home_url
  #   assert_response :success
  #   assert_select "title", "Home | Ruby on Rails Tutorial Sample App"
  # end


end
